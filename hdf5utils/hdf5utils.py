import h5py as h5
import numpy as np

__all__ = ["hdf5File"]


class hdf5File(object):
    def __init__(self, filepath):
        """Init a hdf5 file object handler.
        Args:
            - filepath: the filepath
        """
        self.filepath = filepath
        self.infodict = self.get_info()

    def get_info(self, dataset_only=True):
        """Get dictionary with hdf5 object names and metadata
        Args:
            - dataset_only: if True return path and metadata for datasets only.

        Returns:
            - dictionary with the information
        """

        info_dict = InfoDict(dataset_only)
        try:
            with h5.File(self.filepath, "r") as h5file:
                h5file.visititems(info_dict)
        except FileNotFoundError:
            pass
        return info_dict

    def save_dataset(
        self, dataset_path, data, metadata={}, compression="lzf", mode="a"
    ):
        """Save data to the file with as dataset dataset_path with metadata
        Args:
            - dataset_path: path to dataset in the hdf5file
            - data: the data to save
            - metadata: the metadata to save with the data as dict
            with key= metadata name, value = metadata value. default: {}
            - mode: The mode to open the file in. Default: a, valid are:

                ========  ================================================
                 r        Readonly, file must exist
                 r+       Read/write, file must exist
                 w        Create file, truncate if exists
                 w- or x  Create file, fail if exists
                 a        Read/write if exists, create otherwise (default)
                ========  ================================================

        """
        with h5.File(self.filepath, mode) as h5file:
            data = np.array(data)
            dset = h5file.create_dataset(
                dataset_path, dtype=data.dtype, data=data, compression=compression
            )
            for name, value in metadata.items():
                dset.attrs[name] = value

    def get_array_for_dset(self, dataset_path):
        """Return an numpy array that can store data in dataset_path
        Args:
            - dataset_path: the path to the dataset
        Returns:
            - empty numpy array in the right shape
        """
        return np.zeros(shape=self.infodict[dataset_path]["shape"])

    def load_from_dataset(self, *dataset_paths, arrs=None):
        """Load data from one or more datasets.
        Args:
            - dataset_path: path to the data sets in the hdf5 file
            - arrs: list of arrays to store the data or None
        returns:
            - arrs
        """
        if arrs is None:
            arrs = [self.get_array_for_dset(dset_path) for dset_path in dataset_paths]
        if not len(arrs) == len(dataset_paths):
            raise ValueError(
                "Provide an array for each dataset path " "or don't provide arrays."
            )
        for dset_path, arr in zip(dataset_paths, arrs):
            with h5.File(self.filepath, "r") as h5file:
                _load_from_dataset(h5file, dset_path, arr)
        return arrs

    def __str__(self):
        """Represent this as a string"""
        s = f"hdf5 file stored at {self.filepath} with the following datasets:\n"
        s = s + "\n".join(self.infodict.keys()) + "\n"
        s = s + "For more info inspect this instances infodict attribute.\n"
        return s


def _load_from_dataset(hdf5file, name, arr, source_sel=None, dest_sel=None):
    """Read directly (without copies) to arr.
    Args:
        - hdf5file: the opened file
        - name: the dataset name
        - arr: the array to store the data
        - source_sel: a selection numpy slice created by np.s_ to specify which
        indices to read in the dataset source
        - dest_sel: a selection numpy slice created by np._s to specify the array
        elements to store the data in the destination array

    Returns:
        - arr: the array
    """
    dset = hdf5file[name]
    dset.read_direct(arr, source_sel, dest_sel)
    return arr


def get_info(h5file, dataset_only=True):
    """Get dictionary with hdf5 object names and metadata
    Args:
        - dataset_only: if True return path and metadata for datasets only.

    Returns:
        - dictionary with the information
    """
    info_dict = InfoDict(dataset_only)
    h5file.visititems(info_dict)
    return info_dict


def get_attrs(h5obj):
    """Get metadata for hdf5 object h5obj.
    Args:
        - h5obj: an h5obdject (Group or Dataset)
    Returns:
        - dictionary with keys: metadata names, vals:
            metadata values.
    """
    metadata = {}
    for attname, attval in h5obj.attrs.items():
        metadata[attname] = attval
    return metadata


class InfoDict(dict):
    def __init__(self, dataset_only=True):
        """
        Initialise InfoDict as empty dict.
        Args:
            - dataset_only: if only datasets should be stored.
        """
        self.dataset_only = dataset_only
        super().__init__()

    def __call__(self, h5name, h5obj):
        """
        Add metadata of h5obj to the infodict with key the objects name.
        Args:
            - h5name: path to h5obj
            - h5obj: hdf5 object (group or DataSet)
        Note:
            - this method works excellent with h5file.visititems.
        """
        if isinstance(h5obj, h5.Group):
            if self.dataset_only:
                return
            else:
                self[h5obj.name + "/"] = get_attrs(h5obj)
        # Dataset
        else:
            metadata = get_attrs(h5obj)
            metadata["shape"] = h5obj.shape
            metadata["size"] = h5obj.size
            metadata["dtype"] = h5obj.dtype
            self[h5name] = metadata
        return
